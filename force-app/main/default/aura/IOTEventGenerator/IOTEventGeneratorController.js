({
    doInit: function (component, event, helper) {
        var action = component.get("c.inicio");

        action.setCallback(self, function (a) {
            console.log("Iniciando listado de assets");
            var lista = a.getReturnValue()
            console.log(lista);
            component.set("v.assets", lista);
            component.set("v.assetId", lista[0].External_ID__c);
            console.log("Seleccionado asset: " + component.get("v.assetId"));
        });
        $A.enqueueAction(action);
    },
    changeAsset: function (component, event, helper) {
        var a = component.get("v.assetId");
        console.log("Seleccionado asset: " + a);
    },
    noPower: function (component, event, helper) {
        var action = component.get("c.nopower_event");
        helper.fire(component, helper, action, 'No POW');
    },
    init: function (component, event, helper) {
        var action = component.get("c.init_event");
        helper.fire(component, helper, action, 'Init');
    },
    lowPower: function (component, event, helper) {
        var action = component.get("c.lowpower_event");
        helper.fire(component, helper, action, 'Low Power');
    },
    damage: function (component, event, helper) {
        var action = component.get("c.damage_event");
        helper.fire(component, helper, action, 'Damage');
    },
    rotation: function (component, event, helper) {
        var action = component.get("c.rotation_event");
        helper.fire(component, helper, action, 'Rotation');
    },
    reset: function (component, event, helper) {
        var action = component.get("c.reset_demo");
        helper.fire(component, helper, action, 'Reset');
    }
})