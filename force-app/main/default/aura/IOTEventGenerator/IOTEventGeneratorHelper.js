({
    showToast: function (titulo, msg, tipo) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": tipo,
            "title": titulo,
            "message": msg
        });
        toastEvent.fire();
    },
    fire: function (component, helper, evento, nombre) {
        var asset = component.get("v.assetId");
        var action = evento; //component.get("c.nopower_event");      
        action.setParams({ "asset": asset });
        action.setCallback(self, function (a) {
            var resultado = a.getReturnValue()
            console.log(nombre + ' : ' + resultado);
            if (resultado == 'ok') {
                helper.showToast('', 'Event Fired!', 'success');
            } else {
                helper.showToast('', 'Event Error :(', 'error');
            }
        });
        $A.enqueueAction(action);

    }
})