public class EventGeneratorController {
 	@AuraEnabled
    public static List<Asset> inicio() {
    	return [select id, external_id__c, name,SerialNumber from asset where external_id__c != null order by name];
    } 
    
    @AuraEnabled
    public static string nopower_event(String asset) {
        Tracker_Event__e e = new Tracker_Event__e (Error_code__c='None', Series__c='Gigawatt', Model__c='Midcell', 
                                                   rotation_x__c=260, rotation_z__c=35, power_output__c=0, serial_no__c=asset);
        return fireE(e,'No Power',asset);
        
    }
    
    @AuraEnabled
    public static string init_event(String asset) {
        Tracker_Event__e e = new Tracker_Event__e (Error_code__c='None', Series__c='Gigawatt', Model__c='Midcell', 
                                                   rotation_x__c=260, rotation_z__c=35, power_output__c=2350, serial_no__c=asset);
        return fireE(e,'Init',asset);
        
    }
    
    @AuraEnabled
    public static string lowpower_event(String asset) {
        Tracker_Event__e e = new Tracker_Event__e (Error_code__c='None', Series__c='Gigawatt', Model__c='Midcell', 
                                                   rotation_x__c=260, rotation_z__c=35, power_output__c=1700, serial_no__c=asset);
        return fireE(e,'Low Power',asset);
        
    }
    
	@AuraEnabled
    public static string damage_event(String asset) {
        Tracker_Event__e e = new Tracker_Event__e (Error_code__c='934', Series__c='Gigawatt', Model__c='Midcell', 
                                                   rotation_x__c=260, rotation_z__c=35, power_output__c=2350, serial_no__c=asset);
        return fireE(e,'Damage Cell',asset);
        
    }   
    
    @AuraEnabled
    public static string rotation_event(String asset) {
        Tracker_Event__e e = new Tracker_Event__e (Error_code__c='None', Series__c='Gigawatt', Model__c='Midcell', 
                                                   rotation_x__c=250, rotation_z__c=31, power_output__c=2350, serial_no__c=asset);
        return fireE(e,'Rotation Problem',asset);
        
    } 
    
    @AuraEnabled
    public static string reset_demo(String asset) {
        List<Case> casos = [select id,subject from case where origin='Salesforce IOT' and ContactId=null];
        List<Workorder> ordenes = [select id, subject,  asset.external_id__c,status 
                                   from workorder 
                                   where subject='Salesforce IoT Asset Damaged' and Status='New'];
        
        delete casos;
        List<recordCDC__e > lista_e = new List<recordCDC__e >();
        for (Workorder o : ordenes ) {
            //o.status='Completed';
			recordCDC__e  e = new recordCDC__e  (serial_no__c  = o.asset.external_id__C, recordId__c=o.id);
            lista_e.add(e);
        }
        //update ordenes;
        delete ordenes;
        System.debug('Reset Demo Ok');
        
        return fireListE(lista_e);
    } 
    public static string fireListE(List<SObject> e) {
    	
        List<Database.SaveResult> results = EventBus.publish(e);
        for (Database.SaveResult result : results) {
            if (!result.isSuccess())  {
                for (Database.Error error : result.getErrors()) {
                        System.debug('Error Event reset demo: ' +  error.getStatusCode() +' - '+  error.getMessage());
                    }
                return 'err';    
            }
    	}
        System.debug('Sent '+e.size()+' reset events!');
        return 'ok';
        
    } 
    public static string fireE(SObject e, String name, String asset) {
    	
        Database.SaveResult result = EventBus.publish(e);
        if (result.isSuccess()) {
            System.debug('Event '+name+' success for asset: '+asset);
            return 'ok';
        } else {
            for (Database.Error error : result.getErrors()) {
                    System.debug('Error Event '+name+': ' +  error.getStatusCode() +' - '+  error.getMessage());
                }
        	return 'err';    
        }
        
    } 
    
}